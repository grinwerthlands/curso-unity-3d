﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovientoBola : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(transform.gameObject.name);
        // transform.position.x = 10;
        transform.position = new Vector3(0, 5, 0);
    }

    // Update is called once per frame
    void Update() {
       /* Provoca una excepción:     GameObject vacio = null;
        vacio.transform.position = Vector3.zero;
       */
        bool siFlechaIzquierdaPulsada = Input.GetKey(KeyCode.LeftArrow);

        if (siFlechaIzquierdaPulsada) {

            transform.position = new Vector3(transform.position.x - 0.07f,
                transform.position.y,
                0f);
        }
        bool siFlechaDerechaPulsada = Input.GetKey(KeyCode.RightArrow);


        if (siFlechaDerechaPulsada) {

            transform.position = new Vector3(transform.position.x + 0.07f,
                transform.position.y,
                0f);
        }
    }

}
